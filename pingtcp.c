/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
 * pingtcp - small utility to measure TCP handshake time (torify-friendly)
 *
 * Developed by Oleksandr Natalenko <oleksandr@natalenko.name>
 *
 * Original idea from:
 *
 *   Copyright (C) 2015 Lanet Network
 *   Programmed by Oleksandr Natalenko <o.natalenko@lanet.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <arpa/inet.h>
#include <bsd/string.h>
#include <ctype.h>
#include <dlfcn.h>
#include <errno.h>
#include <libgen.h>
#include <math.h>
#include <netdb.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sysexits.h>
#include <time.h>
#include <unistd.h>

#define Q(x)			#x
#define S(x)			Q(x)
#define APP_VERSION		S(PROJECT_VERSION)
#define APP_YEAR		"2022"
#define APP_PROGRAMMER	"Oleksandr Natalenko"
#define APP_EMAIL		"oleksandr@natalenko.name"

#define pt_zero(A)	memset(&A, 0, sizeof A);

#define PT_ADDRSTRLEN		INET6_ADDRSTRLEN
#define PT_NODE_MAX_LEN		254
#define PT_SERVICESTRLEN	6
#define PT_DEFAULT_PORT		80

static sig_atomic_t enough = 0;

struct pt_ops
{
	int (*socket)(int, int, int);
	int (*getaddrinfo)(const char*, const char*, const struct addrinfo*, struct addrinfo**);
	void (*freeaddrinfo)(struct addrinfo*);
	const char* (*inet_ntop)(int, const void*, char*, socklen_t);
	int (*getnameinfo)(const struct sockaddr*, socklen_t, char*, socklen_t, char*, socklen_t, int);
	int (*connect)(int, const struct sockaddr*, socklen_t);
	int (*close)(int);
};

static struct pt_tor_path
{
	char* path;
} pt_tor_path[] =
{
	{ "/usr/lib/torsocks/libtorsocks.so" },
	{ NULL },
};

static void pt_version(void)
{
	fprintf(stderr, "pingtcp v%s, (C) %s, %s <%s>\n", APP_VERSION, APP_YEAR, APP_PROGRAMMER, APP_EMAIL);
}

static void pt_usage(const char* _basename)
{
	char* __basename = strdup(_basename);
	fprintf(stderr, "Usage: %s [-46cfhivW] <host> [port]\n", basename(__basename));
	free(__basename);
}

static void pt_help(const char* _basename)
{
	pt_version();
	pt_usage(_basename);
	fprintf(stderr, "\nOptions:\n");
	fprintf(stderr, "\t-4, --ipv4\t\t\tuse IPv4\n");
	fprintf(stderr, "\t-6, --ipv6\t\t\tuse IPv6\n");
	fprintf(stderr, "\t-c, --count <count>\t\tstop after <count> handshakes\n");
	fprintf(stderr, "\t-f, --flood\t\t\tflood ping\n");
	fprintf(stderr, "\t-h, --help\t\t\tshow this help\n");
	fprintf(stderr, "\t-i, --interval <interval>\tseconds between each handshake\n");
	fprintf(stderr, "\t-v, --version\t\t\tshow version\n");
	fprintf(stderr, "\t-W, --timeout <timeout>\t\ttime to wait for handshake to succeed (may not work with TOR)\n");
}

static bool pt_isnumber(const char* _string)
{
	char c = '\0';
	while ((c = *_string++))
		if (!isdigit(c))
			return false;
	return true;
}

static void sighandler(int _signo)
{
	(void)_signo;

	enough = 1;
}

static uint64_t timespec_to_ns(struct timespec _timestamp)
{
	return _timestamp.tv_sec * 1000000000ULL + _timestamp.tv_nsec;
}

int main(int _argc, char** _argv)
{
	int arg_i = 0;
	struct pt_ops ops;
	void* tor_path_h = NULL;
	struct addrinfo* res = NULL;
	struct addrinfo hint;
	char node[PT_NODE_MAX_LEN];
	char nodestr[PT_ADDRSTRLEN];
	int ret = 0;
	int err = 0;
	int epfh = 0;
	int port = -1;
	int interval = -1;
	int count = -1;
	int timeout = 1;
	bool flood = false;
	bool ipv4 = false;
	bool ipv6 = false;
	char service[PT_SERVICESTRLEN];
	int sock = 0;
	struct epoll_event epev;
	struct epoll_event repev;
	struct timespec time_start;
	struct timespec time_end;
	struct timespec wall_time_start;
	struct timespec wall_time_end;
	size_t attempt = 0;
	size_t failed = 0;
	size_t succeeded = 0;
	size_t timeouted = 0;
	double loss = 0;
	double wall_time = 0;
	double hs_avg = 0;
	double hs_sqr_sum_ms = 0;
	double hs_mdev = 0;
	uint64_t hs_curr = 0;
	uint64_t hs_sum = 0;
	uint64_t hs_min = UINT64_MAX;
	uint64_t hs_max = 0;

    pt_zero(ops);

	if (signal(SIGINT, sighandler) == SIG_ERR)
	{
		perror("signal");
		exit(EX_OSERR);
	}
	if (signal(SIGTERM, sighandler) == SIG_ERR)
	{
		perror("signal");
		exit(EX_OSERR);
	}

	if (_argc < 2)
	{
		pt_usage(_argv[0]);
		exit(EX_USAGE);
	}

	while (++arg_i < _argc)
	{
		if (_argv[arg_i][0] == '-')
		{
			// switches
			if (!strcmp(_argv[arg_i], "-h") ||
                !strcmp(_argv[arg_i], "--help"))
			{
				pt_help(_argv[0]);
				exit(EX_OK);
			} else if (!strcmp(_argv[arg_i], "-v") ||
						!strcmp(_argv[arg_i], "--version"))
			{
				pt_version();
				exit(EX_OK);
			} else if (!strcmp(_argv[arg_i], "-i") ||
						!strcmp(_argv[arg_i], "--interval"))
			{
				if (_argc <= arg_i)
				{
					fprintf(stderr, "No interval specified");
					exit(EX_USAGE);
				}
				if (!pt_isnumber(_argv[arg_i + 1]))
				{
					fprintf(stderr, "Wrong interval specified");
					exit(EX_USAGE);
				}
				interval = strtoul(_argv[arg_i + 1], NULL, 10);
				arg_i++;
			} else if (!strcmp(_argv[arg_i], "-c") ||
						!strcmp(_argv[arg_i], "--count"))
			{
				if (_argc <= arg_i)
				{
					fprintf(stderr, "No count specified");
					exit(EX_USAGE);
				}
				if (!pt_isnumber(_argv[arg_i + 1]))
				{
					fprintf(stderr, "Wrong count specified");
					exit(EX_USAGE);
				}
				count = strtoul(_argv[arg_i + 1], NULL, 10);
				arg_i++;
			} else if (!strcmp(_argv[arg_i], "-W") ||
						!strcmp(_argv[arg_i], "--timeout"))
			{
				if (_argc <= arg_i)
				{
					fprintf(stderr, "No timeout specified");
					exit(EX_USAGE);
				}
				if (!pt_isnumber(_argv[arg_i + 1]))
				{
					fprintf(stderr, "Wrong timeout specified");
					exit(EX_USAGE);
				}
				timeout = strtoul(_argv[arg_i + 1], NULL, 10);
				arg_i++;
			} else if (!strcmp(_argv[arg_i], "-f") ||
						!strcmp(_argv[arg_i], "--flood"))
			{
				flood = true;
			} else if (!strcmp(_argv[arg_i], "-4") ||
						!strcmp(_argv[arg_i], "--ipv4"))
			{
				ipv4 = true;
			} else if (!strcmp(_argv[arg_i], "-6") ||
						!strcmp(_argv[arg_i], "--ipv6"))
			{
				ipv6 = true;
			}
		} else if (pt_isnumber(_argv[arg_i]))
		{
			// port
			port = strtoul(_argv[arg_i], NULL, 10);
		} else if (strstr(_argv[arg_i], ".onion"))
		{
			// TOR address
			for (struct pt_tor_path* tor_path = pt_tor_path; tor_path->path; tor_path++)
			{
				tor_path_h = dlopen(tor_path->path, RTLD_LAZY);
				if (tor_path_h)
					break;
			}
			if (!tor_path_h)
			{
				fprintf(stderr, "[ERR] No torsocks library found, but .onion address is specified!\n");
				exit(EX_OSERR);
			} else
			{
				*(void**)(&ops.socket) = dlsym(tor_path_h, "socket");
				*(void**)(&ops.getaddrinfo) = dlsym(tor_path_h, "getaddrinfo");
				*(void**)(&ops.freeaddrinfo) = dlsym(tor_path_h, "freeaddrinfo");
				*(void**)(&ops.inet_ntop) = dlsym(tor_path_h, "inet_ntop");
				*(void**)(&ops.getnameinfo) = dlsym(tor_path_h, "getnameinfo");
				*(void**)(&ops.connect) = dlsym(tor_path_h, "connect");
				*(void**)(&ops.close) = dlsym(tor_path_h, "close");
				strlcpy(node, _argv[arg_i], PT_NODE_MAX_LEN);
			}
		} else
		{
			// regular address
			*(void**)(&ops.socket) = dlsym(NULL, "socket");
			*(void**)(&ops.getaddrinfo) = dlsym(NULL, "getaddrinfo");
			*(void**)(&ops.freeaddrinfo) = dlsym(NULL, "freeaddrinfo");
			*(void**)(&ops.inet_ntop) = dlsym(NULL, "inet_ntop");
			*(void**)(&ops.getnameinfo) = dlsym(NULL, "getnameinfo");
			*(void**)(&ops.connect) = dlsym(NULL, "connect");
			*(void**)(&ops.close) = dlsym(NULL, "close");
			strlcpy(node, _argv[arg_i], PT_NODE_MAX_LEN);
		}
	}

	if (port == -1)
	{
		fprintf(stderr, "[WARN] No port specified, defaulting to %d\n", PT_DEFAULT_PORT);
		port = PT_DEFAULT_PORT;
	}

	if (flood && interval != -1)
		fprintf(stderr, "[WARN] Interval is useless in flood mode\n");

	if (ipv4 && ipv6)
	{
		fprintf(stderr, "[WARN] Specifying both IPv4 and IPv6 is useless\n");
		ipv4 = false;
		ipv6 = false;
	}

	if (interval == -1)
		interval = 1;

	if (count == 0)
	{
		fprintf(stderr, "[WARN] Specifying count as zero is useless, defaulting to infinity\n");
		count = -1;
	}

	if (timeout == 0)
	{
		fprintf(stderr, "[WARN] Specifying timeout as zero is useless, defaulting to 1 sec\n");
		timeout = 1;
	}

	if (timeout != 1 && tor_path_h)
		fprintf(stderr, "[WARN] Specifying timeout for TOR address may not work\n");

	if (ipv6 && tor_path_h)
	{
		fprintf(stderr, "[WARN] IPv6 will not work with TOR, using IPv4 instead\n");
		ipv4 = true;
		ipv6 = false;
	}

	pt_zero(hint);
	if (ipv4)
		hint.ai_family = AF_INET;
	else if (ipv6)
		hint.ai_family = AF_INET6;
	hint.ai_socktype = SOCK_STREAM;

	snprintf(service, PT_SERVICESTRLEN, "%d", port);

	ret = ops.getaddrinfo(node, service, &hint, &res);
	if (ret)
	{
		printf("%s\n", gai_strerror(ret));
		exit(EX_SOFTWARE);
	}

	inet_ntop(res->ai_addr->sa_family, res->ai_addr->sa_family == AF_INET ?
			(void*)&((struct sockaddr_in*)res->ai_addr)->sin_addr :
			(void*)&((struct sockaddr_in6*)res->ai_addr)->sin6_addr, nodestr, PT_ADDRSTRLEN);

    printf("PINGTCP %s (%s)\n", node, nodestr);

	if (clock_gettime(CLOCK_MONOTONIC, &wall_time_start) == -1)
	{
		perror("clock_gettime");
		exit(EX_OSERR);
	}

	for (;;)
	{
		attempt++;

		err = 0;

		sock = ops.socket(res->ai_addr->sa_family, res->ai_socktype | SOCK_NONBLOCK, 0);
		if (sock == -1)
		{
			perror("socket");
			exit(EX_OSERR);
		}

		if (clock_gettime(CLOCK_MONOTONIC, &time_start) == -1)
		{
			perror("clock_gettime");
			exit(EX_OSERR);
		}

		ret = ops.connect(sock, res->ai_addr, res->ai_addrlen);

		epfh = epoll_create1(0);
		if (epfh == -1)
		{
			perror("epoll_create1");
			exit(EX_OSERR);
		}

		epev.events = EPOLLOUT;
		ret = epoll_ctl(epfh, EPOLL_CTL_ADD, sock, &epev);
		if (ret == -1)
		{
			perror("epoll_ctl");
			exit(EX_OSERR);
		}
		ret = epoll_wait(epfh, &repev, 1, timeout * 1000);
		if (close(epfh) == -1)
		{
			perror("close");
			exit(EX_OSERR);
		}

		if (ret == -1 && errno == EINTR && enough)
			goto out;
		else if (ret == 0)
		{
			err = ETIMEDOUT;
			timeouted++;
		} else if (ret == -1)
		{
			err = errno;
			failed++;
		} else
			succeeded++;

		if (clock_gettime(CLOCK_MONOTONIC, &time_end) == -1)
		{
			perror("clock_gettime");
			exit(EX_OSERR);
		}

		if (!err)
		{
			hs_curr = timespec_to_ns(time_end) - timespec_to_ns(time_start);
			hs_sum += hs_curr;
			if (hs_curr < hs_min)
				hs_min = hs_curr;
			if (hs_curr > hs_max)
				hs_max = hs_curr;
			hs_sqr_sum_ms += pow(hs_curr / 1000000.0, 2);
		}

		if (ops.close(sock) == -1)
		{
			perror("close");
			exit(EX_OSERR);
		}

		printf("Connect to %s (%s): att=%zu, succ=%zu, fail=%zu, timeo=%zu", node, nodestr, attempt, succeeded, failed, timeouted);
		if (!err)
			printf(", time=%lf ms", hs_curr / 1000000.0);
		else
			printf(" (error: %s)", strerror(err));
		printf("\n");

       	if (enough)
			goto out;

		if (count != -1 && attempt >= (size_t)count)
			goto out;

		if (!flood)
		{
			ret = interval;
			while (ret)
			{
				ret = sleep(ret);
				if (errno == EINTR && enough)
					goto out;
			}
		}
	}

out:
	freeaddrinfo(res);

	if (clock_gettime(CLOCK_MONOTONIC, &wall_time_end) == -1)
	{
		perror("clock_gettime");
		exit(EX_OSERR);
	}

	printf("\n--- %s:%d pingtcp statistics ---\n", node, port);

	loss = (double)(failed + timeouted) / (double)(attempt == 1 ? attempt : attempt - 1) * 100.0;
	wall_time = ((double)timespec_to_ns(wall_time_end) - (double)timespec_to_ns(wall_time_start)) / 1000000.0;

	if (succeeded > 0)
	{
		hs_avg = ((double)hs_sum / (double)succeeded) / 1000000.0;
		hs_mdev = sqrt(hs_sqr_sum_ms / succeeded - pow((hs_sum / 1000000.0) / succeeded, 2));
	} else
	{
		hs_min = 0;
		hs_avg = 0;
		hs_max = 0;
		hs_mdev = 0;
	}

	printf("%lu handshake(s), %lu succeeded, %1.3lf%% loss, time %1.3lf ms\n", attempt, succeeded, loss, wall_time);
	printf("handshake time min/avg/max/mdev = %1.3lf/%1.3lf/%1.3lf/%1.3lf\n", hs_min / 1000000.0, hs_avg, hs_max / 1000000.0, hs_mdev);

	exit(EX_OK);
}
