pingtcp
=======

Description
-----------

Small utility to measure TCP handshake time (torify-friendly).

Compiling
---------

### Prerequisites

* `libbsd` (_for `strlcpy`_)

### Compiling

Use `meson`.

Usage
-----

Typical usage:

`pingtcp kernel.org 443`

See `pingtcp -h` for more options.

Distribution and Contribution
-----------------------------

Distributed under terms and conditions of GNU GPL v3 (only).

The following people are involved in development:

* Oleksandr Natalenko &lt;oleksandr@natalenko.name&gt;

Mail them any suggestions, bugreports and comments.
